# ITMO Lab

Client and micro-server for laboratory work passing.

## Install
```
# make
```

## Client
Dependences: gnome-terminal, screen, zenity.
```
$ itmo_lab
```

## Server
Dependences: python-flask
```
# systemctl daemon-reload
# systemctl start itmo_lab
```

Tools:
* `itmo_lab_setup <set|unset|clean>` -- server setup (firewall, clearing the results directory)
* `itmo_lab_res_prepare <csv_file_with_users>` -- make dirs for labs results
* `itmo_lab_check <lab>` -- `<lab>` works check for all users
* `itmo_lab_report <csv_file_with_users> <output_file> <lab>` -- `<lab>` works report by users


