Name: itmo_lab
Version: 1.1
Release: 1%{?dist}
Summary: Client and micro-server for laboratory work passing.
License: GPLv2+
URL: https://gitlab.com/itmo-infocom/itmo_lab
Source0: https://gitlab.com/itmo-infocom/itmo_lab/-/archive/%{version}/itmo_lab-%{version}.tar.gz

BuildArch:        noarch

%description
Scripts for automatic tests of student's laboratory works

%package client
Requires: gnome-terminal screen zenity
Summary: Client part of itmo_lab
%description client
Client part of itmo_lab

%package server
Requires: python-flask
Summary: Server part of itmo_lab
%description server
Server part of itmo_lab

%prep
%setup -n %{name}-%{version} %{SOURCE0}

%build
##%{__make} %{_smp_mflags}

%install
%{__rm} -fr %{buildroot}
%{__make} PREFIX=%{buildroot} install

%clean
%{__rm} -fr %{buildroot}

%files client
%defattr(-,root,root,-)
%doc README.md
%config /etc/itmo_lab.conf
%{_bindir}/*
%{_datadir}/applications/*

%files server
%defattr(-,root,root,-)
%doc README.md
%config /etc/itmo_lab.conf
%config /etc/pki/tls/certs/itmo_lab
%{_sbindir}/*
%config /etc/systemd/system/*
%{_var}/labs
%attr(755, itmo_lab, itmo_lab) %{_var}/spool/labs

%post client
update-desktop-database &> /dev/null

%postun client
update-desktop-database &> /dev/null

%pre server
id itmo_lab >/dev/null 2>&1 || adduser itmo_lab

%post server
systemctl daemon-reload
#itmo_lab_setup set

%preun server
#itmo_lab_setup unset

%postun server
systemctl daemon-reload

%changelog
* Wed Sep 30 2020 Oleg Sadov <sadov at naulinux.ru> 1.1-1
- New set of files for Lab -- title, pre, post, test.

* Sat Sep 19 2020 Oleg Sadov <sadov at naulinux.ru> 1.0-1
- Server run by itmo_lab user.

* Tue Sep 15 2020 Oleg Sadov <sadov at naulinux.ru> 0.9-1
- Client fixes. Users CSV files support.

* Mon Sep 14 2020 Oleg Sadov <sadov at naulinux.ru> 0.8-1
- Added status checking and reporting for lab works.

* Mon Sep 14 2020 Oleg Sadov <sadov at naulinux.ru> 0.7-1
- Added HTTPS support.

* Sat Sep 12 2020 Oleg Sadov <sadov at naulinux.ru> 0.6-1
- Changed host from localhost to itmo.naulinux.ru

* Fri Sep 11 2020 Vladimir Titov <tit at astro.spbu.ru> 0.5-1
- Initial spec file
