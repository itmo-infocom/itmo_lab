PREFIX ?= ""

install: install_client install_server
	mkdir -p $(PREFIX)/etc/
	cp itmo_lab.conf $(PREFIX)/etc/

install_client:
	mkdir -p $(PREFIX)/usr/bin
	cp client/* $(PREFIX)/usr/bin
	mkdir -p $(PREFIX)/usr/share/applications/
	cp itmo_lab.desktop $(PREFIX)/usr/share/applications/

install_server:
	mkdir -p $(PREFIX)/usr/sbin
	cp server/* $(PREFIX)/usr/sbin
	mkdir -p $(PREFIX)/etc/systemd/system/
	cp server/itmo_lab.service $(PREFIX)/etc/systemd/system/
	mkdir -p $(PREFIX)/var/labs
	mkdir -p $(PREFIX)/var/spool/labs
	mkdir -p $(PREFIX)/etc/pki/tls/certs/itmo_lab
