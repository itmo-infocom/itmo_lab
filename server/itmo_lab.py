import glob
import os
import string
import subprocess

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from flask import Flask, request

app = Flask(__name__)

labs_path='/var/labs/'
rslt_path='/var/spool/labs/'

@app.route('/<user>')
def list(user):
    labs = os.listdir(labs_path)
    labs.sort()
    for l in range(0, len(labs)):
        ures = rslt_path + '%s/%s/' % (labs[l], user)
        if not os.path.exists(ures):
            #os.makedirs(ures)
            return "This user is not registered in this course"

        title = ""
        res = ""
        sign = ""
        fls = glob.glob(labs_path + '%s/title' % labs[l])
        if len(fls):
            f = open(fls[0])
            title = f.read().strip()
        fls = glob.glob(rslt_path + '%s/%s/res?' % (labs[l], user))
        if len(fls):
            sign = fls[0][-1]
            if sign == '+':
                res = ' (passed)'
            elif sign == '-':
                res = ' (not passed)'

        labs[l] += "%s '%s%s'" % (sign, title, res)
    labs = ' '.join(labs)
    return labs

@app.route('/get/<lab>')
def get(lab):
    os.chdir(labs_path + lab)
    pre = labs_path + '%s/pre' % lab
    post = labs_path + '%s/post' % lab
    scripts = rslt_path + '%s/scripts.tgz' % lab
    mpre = os.path.getmtime(pre)
    mpost = os.path.getmtime(post)

    if os.path.isfile(scripts): 
        mscripts = os.path.getmtime(scripts)
        if mscripts <= mpre or mscripts <= mpost:
            if os.system('tar cvzf %s pre post' % scripts) != 0:
                return "Error creating tar"
    else:
        if os.system('tar cvzf %s pre post' % scripts) != 0:
            return "Error creating tar"

    scripts = open(scripts)
    scripts = scripts.read()
    return scripts

@app.route('/put/<lab>/<user>', methods=['POST'])
def put(lab, user):
    ures = rslt_path + '%s/%s/' % (lab, user)

    os.chdir(ures)
    f = request.files['file']
    out = ures + 'out.tgz'
    f.save(out)

    if not os.path.exists(ures):
        #os.makedirs(ures)
        return "This user is not registered in this course"

    res = glob.glob(rslt_path + '%s/%s/res?' % (lab, user))
    if len(res):
        if res[0][-1] == '+':
            rslt = 'passed'
        elif res[0][-1] == '-':
            rslt = 'not passed'
        return "You have already completed lab %s (%s)\n" % (lab, rslt)

    proc = subprocess.Popen(['sh', '%s/%s/test'%(labs_path, lab), out],
        stdout = subprocess.PIPE,
        stderr = subprocess.PIPE,
    )
    stdout, stderr = proc.communicate()
 
    out += """
CMD=%s
EXIT=%d
STDOUT:
%s
STDERR:
%s
""" % ('sh %s/%s/test %s' % (labs_path, lab, out),
       proc.returncode, stdout, stderr)

    res = open(ures + 'res', "w+")
    res.write(out)

    if proc.returncode == 0:
        out = "Lab %s passed\n" % lab
    else:
        out = "Lab %s have not passed\n" % lab


    return out

if __name__ == '__main__':
    port=string.atoi(os.getenv('ITMO_LAB_PORT'))
    if os.getenv('ITMO_LAB_PROT') == "https":
        context=('/etc/pki/tls/certs/itmo_lab/cert.pem',
                 '/etc/pki/tls/certs/itmo_lab/key.pem')
        app.run(host= '0.0.0.0', port=port, ssl_context=context, debug=True)
    else:
        app.run(host= '0.0.0.0', port=port, debug=True)
